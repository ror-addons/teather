	local Version = 1.0
	
	if not teather then teather = {} end
	local UIScale = InterfaceCore.GetScale()
    local ResolutionScale = InterfaceCore.GetResolutionScale()	
	local GlobalScale = SystemData.Settings.Interface.globalUiScale	
	local ScreenWidthX,ScreenHeightY = GetScreenResolution()	
	local LineLength = 58
	local NumberLines = 0
	local MaxLines = ((((ScreenWidthX+ScreenHeightY/2)/ResolutionScale)/GlobalScale)/LineLength)/2	
	local ObjecOffset = 0.9962
	local AnchorOffset = -30
	local LatestDir2
	local LatestDir3	
	local MAX_MAP_POINTS = 511
	local DISTANCE_FIX_COEFFICIENT = 1 / 1.06
	
	local GUARDED_APPLY_ID = 1			--when another player add guard to you
	local GUARDED_REMOVE_ID = 2			--when another players guard removes from you
	local GUARDING_APPLY_ID = 3			--when you add guard to another player
	local GUARDING_REMOVE_ID = 4			--when your guard removes from another player
	
	local MapPointTypeFilter = {
		[SystemData.MapPips.PLAYER] = true,
		[SystemData.MapPips.GROUP_MEMBER] = true,
		[SystemData.MapPips.WARBAND_MEMBER] = true,
		[SystemData.MapPips.DESTRUCTION_ARMY] = true,
		[SystemData.MapPips.ORDER_ARMY] = true
	}	
	
	
	teather.DefaultColor = {R=255,G=255,B=255}
	teather.CloseColor = {R=50,G=255,B=50}
	teather.MidColor = {R=100,G=100,B=255}
	teather.FarColor = {R=255,G=50,B=50}
	teather.DistantColor = {R=125,G=125,B=125}
	
function teather.init()
	SelfID = GameData.Player.worldObjNum
	CreateWindow("TeatherSelfWindow", false)
	CreateWindow("TeatherTargetWindow", false)
	CreateWindow("TeatherOffGuardSelfWindow", false)
	

	LayoutEditor.RegisterWindow( "TeatherSelfWindow", L"Teather Window", L"Teather Window", false, false, false)
	LayoutEditor.RegisterEditCallback(teather.OnLayoutEditorFinished)
	
	CreateWindowFromTemplate("TeatherOffGuardTargetWindow", "TeatherTargetWindow", "Root")	
	
	
	IsTeathered = 0.1
	IsTeatheredOffGuard = 0.1	
	NumberOfGuards = 0	
	for i=1,MaxLines do	
		CreateWindowFromTemplate("TeatherLine"..i, "LineTemplate", "TeatherSelfWindow")	

		WindowSetShowing("TeatherLine"..i.."Line",true)
		WindowSetAlpha("TeatherLine"..i.."Line",0)
		
		
		CreateWindowFromTemplate("TeatherOffGuardLine"..i, "LineTemplate2", "TeatherOffGuardSelfWindow")	

		WindowSetShowing("TeatherOffGuardLine"..i.."Line",true)
		WindowSetAlpha("TeatherOffGuardLine"..i.."Line",0)		
	end	




	CreateWindowFromTemplate("TeatherLineEnd", "LineTemplate", "TeatherTargetWindow")	
	CreateWindowFromTemplate("TeatherOffGuardLineEnd", "LineTemplate2", "TeatherOffGuardTargetWindow")	
	WindowSetParent("TeatherOffGuardLineEnd","TeatherOffGuardTargetWindow")

	local texture, x, y, disabledTexture = GetIconData(tonumber(80))
	CircleImageSetTexture( "TeatherOffGuardTargetWindowIcon", "GuardIcon", 32, 32 )
	CircleImageSetTexture( "TeatherOffGuardSelfWindowIcon", "GuardIcon", 32, 32 )	

	UIScale = InterfaceCore.GetScale()
    ResolutionScale = InterfaceCore.GetResolutionScale()	
	GlobalScale = SystemData.Settings.Interface.globalUiScale	
	ScreenWidthX,ScreenHeightY = GetScreenResolution()	
	LineLength = 58
	MaxLines = ((((ScreenWidthX+ScreenHeightY/2)/ResolutionScale)/GlobalScale)/LineLength)/2	
	ObjecOffset = 0.9962
	LatestDir = math.random(360)
	LatestDir2 = math.random(360)
	LatestDir3 = math.random(360)	
	teather.TargetID = 0
	teather.TargetName = L""
	teather.TargetInfo = {}

	teather.OffGuardID = 0
	teather.OffGuardName = L""
	WindowSetShowing("TeatherOffGuardTargetWindow",false) 
	WindowSetShowing("TeatherOffGuardSelfWindow",false) 
	WindowSetShowing("TeatherOffGuardSelfWindowGlow",false)	
	WindowSetShowing ("TeatherOffGuardTargetWindowGlow", false)	
	if LibGuard then 
	LibGuard.Register_Callback(teather.Libguard_Toggle)		
	end
	
	AnimatedImageStartAnimation ("TeatherSelfWindowGlow", 0, true, true, 0)	
	AnimatedImageStartAnimation ("TeatherTargetWindowGlow", 0, true, true, 0)	
	AnimatedImageStartAnimation ("TeatherOffGuardSelfWindowGlow", 0, true, true, 0)	
	AnimatedImageStartAnimation ("TeatherOffGuardTargetWindowGlow", 0, true, true, 0)		
	
end

function teather.OnLayoutEditorFinished( editorCode )
	if( editorCode == LayoutEditor.EDITING_END ) then
		WindowSetShowing("TeatherSelfWindow",false)
		WindowSetShowing("TeatherTargetWindow",false)		
		WindowSetShowing("TeatherOffGuardSelfWindow",false)				
	end
end


--Attach/deatach and Show/Hide the teather windows
function teather.GetIDs()
	local PlayerName = teather.FixString(GameData.Player.name)
	teather.TargetID = TargetInfo:UnitEntityId("selffriendlytarget")
	teather.TargetName = teather.FixString(TargetInfo:UnitName("selffriendlytarget"))

	if teather.TargetName ~= L"" and not (teather.TargetName == PlayerName) then 
	--	DetachWindowFromWorldObject("TeatherTargetWindow",TargetID)
		IsTeathered = 0.1
		WindowSetShowing("TeatherTargetWindow",true) 
		WindowSetShowing("TeatherSelfWindow",true) 
	else
	--	DetachWindowFromWorldObject("TeatherTargetWindow",TargetID)
		IsTeathered = 0.1
		WindowSetShowing("TeatherTargetWindow",false) 
		WindowSetShowing("TeatherSelfWindow",false)
		teather.TargetID = 0	
	end
	
--	teather.update()
end

function teather.GetIDs2()
	local PlayerName = teather.FixString(GameData.Player.name)
	teather.OffGuardID = TargetInfo:UnitEntityId("selffriendlytarget")
	teather.OffGuardName = teather.FixString(TargetInfo:UnitName("selffriendlytarget"))

	if teather.OffGuardName ~= L"" and not (teather.OffGuardName == PlayerName) then 
	--	DetachWindowFromWorldObject("TeatherTargetWindow",TargetID)
		IsTeatheredOffGuard = 0.1
		WindowSetShowing("TeatherOffGuardTargetWindow",true) 
		WindowSetShowing("TeatherOffGuardSelfWindow",true) 
	else
	--	DetachWindowFromWorldObject("TeatherTargetWindow",TargetID)
		IsTeatheredOffGuard = 0.1
		WindowSetShowing("TeatherOffGuardTargetWindow",false) 
		WindowSetShowing("TeatherOffGuardSelfWindow",false) 
		teather.OffGuardID = 0
	end
	
--	teather.update()
end



function teather.update(timeElapsed)

local IsTargetGuarded = LibGuard.GuarderData.Name == teather.TargetName
local distance = 0

if teather.TargetName ~= nil and teather.TargetName ~= L"" then

	--Did i mentioned i hate math? because im just so terrible at it...
	--Only check for distance if in a party,warband or scenario
	if IsWarBandActive() or PartyUtils.IsPartyActive() or GameData.Player.isInScenario or GameData.Player.isInSiege then
		distance = LibGuard.GuarderData.distance
		teather.TargetID = LibGuard.GuarderData.ID
		teather.TargetInfo = LibGuard.GuarderData.Info
	end
	
	WindowSetScale("TeatherSelfWindow",UIScale)
	--WindowSetScale("TeatherTargetWindow",UIScale*(1-(distance/700)))
	--WindowSetScale("TeatherLineEndLine",UIScale)
	
	
	local IsDistant = true
	if LibGuard.GuarderData.Info ~= nil and LibGuard.GuarderData.Info ~= 0 then
	IsDistant = LibGuard.GuarderData.Info.isDistant
	end

	if IsDistant == true then 
		teather.Color = teather.DistantColor
	else
		if distance <= 30 and distance >= 0 then
			teather.Color = teather.CloseColor
		elseif distance > 30 and distance <= 50 then
			teather.Color = teather.MidColor
		elseif distance > 50 then
			teather.Color = teather.FarColor
		else
			teather.Color = teather.DistantColor
		end
	end
	
	
		for i=1,MaxLines do
			WindowSetAlpha("TeatherLine"..i.."Line",0)
		end
			WindowSetAlpha("TeatherLineEndLine",0)
			local ShowLines = 0
			if WindowGetShowing("TeatherTargetWindow") == true then ShowLines = 1 end
			
			if  teather.TargetID ~= nil then
				MoveWindowToWorldObject("TeatherTargetWindow", teather.TargetID, ObjecOffset)--0.9962 for player 0.9975
				ForceUpdateWorldObjectWindow(teather.TargetID,"TeatherTargetWindow")
			end
			--WindowSetOffsetFromParent("TeatherTargetWindow",0,AnchorOffset)	
				
			local TargetTempWindowX,TargetTempWindowY = WindowGetScreenPosition("TeatherTargetWindow")
			local PlayerTempWindowX,PlayerTempWindowY = WindowGetScreenPosition("TeatherSelfWindow")
			local xDiff = PlayerTempWindowX-TargetTempWindowX
			local yDiff = PlayerTempWindowY-TargetTempWindowY
			local Degrees = (math.atan2(yDiff,xDiff)*(180 / math.pi)-90)
			local DistX = 	(PlayerTempWindowX - TargetTempWindowX)
			local DistY = 	(PlayerTempWindowY - TargetTempWindowY)
			local TestDistance = (math.sqrt(DistX*DistX+DistY*DistY))/UIScale		
			NumberLines = math.floor(TestDistance/LineLength)+1
	
	
			for i=1,NumberLines do
				local xDiff2 = PlayerTempWindowX-TargetTempWindowX
				local yDiff2 = PlayerTempWindowY-TargetTempWindowY
				local Degrees2 = (math.atan2(yDiff2,xDiff2)*(180 / math.pi)-90)

				--only do a sprite roation if there is change in the direction
				if LatestDir2 ~= Degrees2 then				
					local radius2 = (LineLength*i)-35
					local xPos2 = radius2 * math.cos(math.rad(Degrees2-90)) 
					local yPos2 = radius2 * math.sin(math.rad(Degrees2-90)) 
					DynamicImageSetRotation("TeatherLine"..i.."Line",Degrees2)				
					WindowClearAnchors( "TeatherLine"..i )
					WindowAddAnchor( "TeatherLine"..i , "topleft", "TeatherSelfWindow", "topleft", xPos2+27,yPos2-2)			
				end					
			
				WindowSetTintColor("TeatherLine"..i.."Line",teather.Color.R,teather.Color.G,teather.Color.B)
				if IsTeathered <= 0 then 
					WindowSetAlpha("TeatherLine"..i.."Line",ShowLines)
				end				
			end	
			
			LatestDir2 = Degrees2
--Why Alpha the lines instead of Hiding/showing?, Becasue Hiding/showing is slower that doing Alpha (perhaps it has to redo the drawing routine and go throu eventhandler?), took me a while to figure this out because it casued a lot of sprite artifacts when rotating
			
			for a=(NumberLines),MaxLines do	
				WindowSetAlpha("TeatherLine"..a.."Line",0)
			end
			
		
				local xPosEnd = 40 * math.cos(math.rad(Degrees-270)) 
				local yPosEnd = 40 * math.sin(math.rad(Degrees-270)) 
				DynamicImageSetRotation("TeatherLineEndLine",Degrees)
				WindowClearAnchors( "TeatherLineEnd" )
				WindowAddAnchor( "TeatherLineEnd" , "topleft", "TeatherTargetWindow", "topleft", xPosEnd+27,yPosEnd-2)	

			
			WindowSetTintColor("TeatherLineEndLine",teather.Color.R,teather.Color.G,teather.Color.B)
			WindowSetTintColor("TeatherSelfWindowCircle",teather.Color.R,teather.Color.G,teather.Color.B)
			WindowSetTintColor("TeatherTargetWindowCircle",teather.Color.R,teather.Color.G,teather.Color.B)						
			
			WindowSetTintColor("TeatherSelfWindowIcon",teather.Color.R,teather.Color.G,teather.Color.B)
			WindowSetTintColor("TeatherTargetWindowIcon",teather.Color.R,teather.Color.G,teather.Color.B)						


			WindowSetTintColor("TeatherSelfWindowGlow",teather.Color.R,teather.Color.G,teather.Color.B)
			WindowSetTintColor("TeatherTargetWindowGlow",teather.Color.R,teather.Color.G,teather.Color.B)			

			if IsTeathered > 0 then 
				IsTeathered = IsTeathered-timeElapsed
			else
							if TestDistance <= (LineLength+5) then
			WindowSetAlpha("TeatherLineEndLine",0)
			WindowSetAlpha("TeatherLine1Line",0)
			else
			WindowSetAlpha("TeatherLineEndLine",ShowLines)
			WindowSetAlpha("TeatherLine1Line",ShowLines)
			end
			end	

	
	local S_x,S_y = WindowGetScreenPosition("TeatherTargetWindow")	
	if  (S_x+ S_y) == 0 or (WindowGetShowing("TeatherSelfWindow")) == false or (IsDistant == true) then WindowSetShowing("TeatherTargetWindow",false) end
		LatestDir2 = Degrees2
--	end
	if LibGuard.GuarderData.IsGuarding == true and IsTargetGuarded then
	local texture, x, y, disabledTexture = GetIconData(tonumber(80))
	CircleImageSetTexture( "TeatherTargetWindowIcon", "GuardIcon", 32, 32 )
	CircleImageSetTexture( "TeatherSelfWindowIcon", "GuardIcon", 32, 32 )
	else
	local texture, x, y, disabledTexture = GetIconData(tonumber(32))
	CircleImageSetTexture( "TeatherTargetWindowIcon", "GuardIcon", 32, 32 )
	CircleImageSetTexture( "TeatherSelfWindowIcon", "GuardIcon", 32, 32 )
	end

local ShowGlow = ((tostring(LibGuard.GuarderData.Name) == tostring(teather.OffGuardName)) and (Check_XGuard()) )
	WindowSetTintColor("TeatherSelfWindowGlow",teather.Color.R,teather.Color.G,teather.Color.B)	
	WindowSetTintColor("TeatherTargetWindowGlow",teather.Color.R,teather.Color.G,teather.Color.B)	
	WindowSetShowing("TeatherSelfWindowGlow",ShowGlow and IsTargetGuarded)	
	WindowSetShowing ("TeatherTargetWindowGlow", ShowGlow and IsTargetGuarded)	

end

teather.update2(timeElapsed)
end

--This is for OffGuard Tethering
function teather.update2(timeElapsed)

	local IsDistant = true
	if teather.OffGuardInfo ~= nil and teather.OffGuardInfo ~= 0 then
	IsDistant = teather.OffGuardInfo.isDistant
	end
	
if teather.OffGuardName ~= nil and teather.OffGuardName ~= L"" then
local IsTargetGuarded = LibGuard.GuarderData.Name == teather.TargetName
local distance = 0 
local Color = teather.DefaultColor

		if LibGuard.registeredGuards[tostring(teather.OffGuardName)] ~= nil then
		distance = LibGuard.registeredGuards[tostring(teather.OffGuardName)].distance
		
	if IsDistant == true then
		Color = teather.DistantColor
	else
		if distance <= 30 and distance >= 0 then
			Color = teather.CloseColor
		elseif distance > 30 and distance <= 50 then
			Color = teather.MidColor
		elseif distance > 50 then
			Color = teather.FarColor
		else
			Color = teather.DistantColor
		end
	end	

		else
			WindowSetShowing("TeatherOffGuardSelfWindowLabel",false)
			WindowSetShowing("TeatherOffGuardSelfWindowLabelBG",false)						
		end

	
	teather.OffGuardInfo = LibGuard.GetIdFromName(tostring(teather.OffGuardName),3)
	


	
	
		for i=1,MaxLines do
			WindowSetAlpha("TeatherOffGuardLine"..i.."Line",0)
		end
			WindowSetAlpha("TeatherOffGuardLineEndLine",0)
			local ShowLines = 0
			if WindowGetShowing("TeatherOffGuardTargetWindow") == true and (Check_XGuard() == false) then ShowLines = 1 end
			if teather.OffGuardID ~= nil then
				MoveWindowToWorldObject("TeatherOffGuardTargetWindow", teather.OffGuardID, ObjecOffset)--0.9962 for player 0.9975
				ForceUpdateWorldObjectWindow(teather.OffGuardID,"TeatherOffGuardTargetWindow")
			end
			--WindowSetOffsetFromParent("TeatherTargetWindow",0,AnchorOffset)	
				
			local TargetTempWindowX,TargetTempWindowY = WindowGetScreenPosition("TeatherOffGuardTargetWindow")
			local PlayerTempWindowX,PlayerTempWindowY = WindowGetScreenPosition("TeatherOffGuardSelfWindow")
			local xDiff = PlayerTempWindowX-TargetTempWindowX
			local yDiff = PlayerTempWindowY-TargetTempWindowY
			local Degrees = (math.atan2(yDiff,xDiff)*(180 / math.pi)-90)
			local DistX = 	(PlayerTempWindowX - TargetTempWindowX)
			local DistY = 	(PlayerTempWindowY - TargetTempWindowY)
			local TestDistance = (math.sqrt(DistX*DistX+DistY*DistY))/UIScale		
			NumberLines = math.floor(TestDistance/LineLength)+1
	
			for i=1,NumberLines do
				local xDiff2 = PlayerTempWindowX-TargetTempWindowX
				local yDiff2 = PlayerTempWindowY-TargetTempWindowY
				local Degrees2 = (math.atan2(yDiff2,xDiff2)*(180 / math.pi)-90)

				--only do a sprite roation if there is change in the direction
				if LatestDir3 ~= Degrees2 then				
					local radius2 = (LineLength*i)-35
					local xPos2 = radius2 * math.cos(math.rad(Degrees2-90)) 
					local yPos2 = radius2 * math.sin(math.rad(Degrees2-90)) 
					DynamicImageSetRotation("TeatherOffGuardLine"..i.."Line",Degrees2)				
					WindowClearAnchors( "TeatherOffGuardLine"..i )
					WindowAddAnchor( "TeatherOffGuardLine"..i , "topleft", "TeatherOffGuardSelfWindow", "topleft", xPos2+27,yPos2-2)			
				end	
			
				WindowSetTintColor("TeatherOffGuardLine"..i.."Line",Color.R,Color.G,Color.B)
				if IsTeatheredOffGuard <= 0 then 
					WindowSetAlpha("TeatherOffGuardLine"..i.."Line",ShowLines)
				end				
			end	
			

--	end			
			
			LatestDir3 = Degrees2
--Why Alpha the lines instead of Hiding/showing?, Becasue Hiding/showing is slower that doing Alpha (perhaps it has to redo the drawing routine and go throu eventhandler?), took me a while to figure this out because it casued a lot of sprite artifacts when rotating
			
			for a=(NumberLines),MaxLines do	
				WindowSetAlpha("TeatherOffGuardLine"..a.."Line",0)
			end
			
		
				local xPosEnd = 40 * math.cos(math.rad(Degrees-270)) 
				local yPosEnd = 40 * math.sin(math.rad(Degrees-270)) 
				DynamicImageSetRotation("TeatherOffGuardLineEndLine",Degrees)
				WindowClearAnchors( "TeatherOffGuardLineEnd" )
				WindowAddAnchor( "TeatherOffGuardLineEnd" , "topleft", "TeatherOffGuardTargetWindow", "topleft", xPosEnd+27,yPosEnd-2)	

			
			WindowSetTintColor("TeatherOffGuardLineEndLine",Color.R,Color.G,Color.B)
			WindowSetTintColor("TeatherOffGuardSelfWindowCircle",Color.R,Color.G,Color.B)
			WindowSetTintColor("TeatherOffGuardTargetWindowCircle",Color.R,Color.G,Color.B)						
			WindowSetTintColor("TeatherOffGuardSelfWindowIcon",Color.R,Color.G,Color.B)
			WindowSetTintColor("TeatherOffGuardTargetWindowIcon",Color.R,Color.G,Color.B)		


			WindowSetTintColor("TeatherOffGuardSelfWindowGlow",Color.R,Color.G,Color.B)		
			WindowSetTintColor("TeatherOffGuardTargetWindowGlow",Color.R,Color.G,Color.B)	


			if IsTeatheredOffGuard > 0 then 
				IsTeatheredOffGuard = IsTeatheredOffGuard-timeElapsed
			else
							if TestDistance <= (LineLength+5) then
			WindowSetAlpha("TeatherOffGuardLineEndLine",0)
			WindowSetAlpha("TeatherOffGuardLine1Line",0)
			else
			WindowSetAlpha("TeatherOffGuardLineEndLine",ShowLines)
			WindowSetAlpha("TeatherOffGuardLine1Line",ShowLines)
			end
			end	
		

			
		 	--WindowSetShowing("TeatherTargetWindow",WindowGetShowing("TeatherSelfWindow"))
				LatestDir3 = Degrees2	
end
	if (LibGuard.registeredGuards == nil or (NumberOfGuards == 0) or (tostring(LibGuard.GuarderData.Name) == tostring(teather.OffGuardName))) and (IsTargetGuarded) then
	WindowSetShowing("TeatherOffGuardSelfWindow",false)
	else
	WindowSetShowing("TeatherOffGuardSelfWindow",true)
	teather.OffTarget()
	end	

	local S_x,S_y = WindowGetScreenPosition("TeatherOffGuardTargetWindow")	
	if  ((S_x+ S_y) == 0 or (WindowGetShowing("TeatherOffGuardSelfWindow")) == false) or (IsDistant == true) then WindowSetShowing("TeatherOffGuardTargetWindow",false) end


local ShowGlow = ((tostring(LibGuard.GuarderData.Name) == tostring(teather.OffGuardName)) and (Check_XGuard()) )

	WindowSetShowing("TeatherOffGuardSelfWindowGlow",ShowGlow and not IsTargetGuarded)	
	WindowSetShowing ("TeatherOffGuardTargetWindowGlow", ShowGlow and not IsTargetGuarded)	

--	end

end

function teather.OffTarget()
NumberOfGuards = 0
local Range = 999999
teather.OffGuardName = L""
	for k, v in pairs( LibGuard.registeredGuards ) do	
		NumberOfGuards = NumberOfGuards+1
	end
			
			
	for key, value in pairs( LibGuard.registeredGuards ) do	
		if LibGuard.registeredGuards[key].distance ~= nil and LibGuard.registeredGuards[key].distance < Range then
		Range = LibGuard.registeredGuards[key].distance		
		teather.OffGuardID = LibGuard.registeredGuards[key].ID
		teather.OffGuardName = towstring(key)		
		end
		
	end			
	
	
if (NumberOfGuards == 0) then	
		IsTeatheredOffGuard = 0.1
		WindowSetShowing("TeatherOffGuardTargetWindow",false) 
		WindowSetShowing("TeatherOffGuardSelfWindow",false)
		teather.OffGuardID = 0	
		teather.OffGuardName = L""
end	
return		
end





--This is just for debuggiing Info
function teather.GetInfo()
	local TargetParentX,TargetParentY = WindowGetScreenPosition("TeatherTargetWindow")
	local SelfParentX,SelfParentY = WindowGetScreenPosition("TeatherSelfWindow")
	local xDiff = SelfParentX-TargetParentX
	local yDiff = SelfParentY-TargetParentY
	teather.Degrees = (math.atan2(yDiff,xDiff)*(180 / math.pi)-90)
	local DistX = 	SelfParentX - TargetParentX
	local DistY = 	SelfParentY - TargetParentY		
	teather.Distance = math.sqrt(DistX*DistX+DistY*DistY)
	d(L"X:"..towstring(SelfParentX-TargetParentX)..L" Y:"..towstring(SelfParentY-TargetParentY))
	d(L"Deg: "..towstring(teather.Degrees))
	d(L"Dist: "..towstring(teather.Distance))
	d(L"GlobalScale: "..towstring(GlobalScale))
	d(L"ResolutionScale: "..towstring(ResolutionScale))
	d(L"Screen Width: "..towstring(ScreenWidthX))	
	d(L"Screen Height: "..towstring(ScreenHeightY))	
	d(L"Lines? "..towstring(((((ScreenWidthX+ScreenHeightY/2)/ResolutionScale)/GlobalScale)/55)/2))
end



function teather.FixString (str)
	if (str == nil) then return nil end
	local str = str
	local pos = str:find (L"^", 1, true)
	if (pos) then str = str:sub (1, pos - 1) end	
	return str
end


function teather.Libguard_Toggle(state,GuardedName,GuardedID)
--d(L"State:"..towstring(state)..L" Name:"..towstring(GuardedName)..L" ID:"..towstring(GuardedID))
		if state == GUARDED_APPLY_ID then
		teather.OffGuardID = GuardedID
		teather.OffGuardName = towstring(GuardedName)		
		IsTeatheredOffGuard = 0.1
		WindowSetShowing("TeatherOffGuardTargetWindow",true) 
		WindowSetShowing("TeatherOffGuardSelfWindow",true) 
		teather.OffTarget()		
		elseif state == GUARDED_REMOVE_ID then
		teather.OffTarget()		
		elseif state == GUARDING_REMOVE_ID then
			teather.TargetName = L""
			IsTeathered = 0.1
			WindowSetShowing("TeatherTargetWindow",false) 
			WindowSetShowing("TeatherSelfWindow",false)
			teather.TargetID = 0			
		elseif state == GUARDING_APPLY_ID then
			teather.TargetName = towstring(GuardedName)
			IsTeathered = 0.1
			WindowSetShowing("TeatherTargetWindow",true) 
			WindowSetShowing("TeatherSelfWindow",true) 
			teather.GetIDs()			
		end			
end





